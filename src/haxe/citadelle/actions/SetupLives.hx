package citadelle.actions;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import Std.int;
import etwin.Obfu;

class SetupLives implements IAction {
  public var name(default, null): String = Obfu.raw("setupLives");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
  	var game: GameMode = ctx.getGame();
    var lives: Null<Int> = ctx.getOptInt(Obfu.raw("lives")).or(0);

    if(lives == null) lives = 0;

  	var playerList = game.getPlayerList();
    if(playerList.length > 1) {
      lives = int(lives / 2);
    }

    for (i in 0...playerList.length) {
      var p = playerList[i];
      p.lives = lives;
      game.gi.setLives(p.pid, lives);
    }

    return false;
  }
}