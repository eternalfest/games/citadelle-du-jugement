import bugfix.Bugfix;
import debug.Debug;
import citadelle.Items;
import citadelle.Actions;
import hf.Hf;
import merlin.Merlin;
import patchman.IPatch;
import patchman.Patchman;
import pause_limiter.PauseLimiter;
import vault.Vault;
import game_params.GameParams;
import framecounter.FrameCounter;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
    items: Items,
    actions: Actions,
    game_params: GameParams,
    pauseLimiter: PauseLimiter,
    vault: Vault,
    framecounter: FrameCounter,
    darknessLevels: atlas.props.Darkness,
    atlas: atlas.Atlas,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
