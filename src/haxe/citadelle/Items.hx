package citadelle;

import patchman.PatchList;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.ISpec;

import citadelle.items.Eternal;

@:build(patchman.Build.di())
class Items {
  @:diExport
  public var items(default, null): FrozenArray<ISpec>;

  @:diExport
    public var patch(default, null): IPatch;

    public function new(patches: Array<IPatch>) {
      items = cast FrozenArray.of(
          new Eternal()
      );

      this.patch = new PatchList(patches);
    }
}