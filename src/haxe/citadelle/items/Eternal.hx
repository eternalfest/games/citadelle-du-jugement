package citadelle.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;

class Eternal implements ISpec {
  public var id(default, null): Int = 118;

  public function new(): Void {
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.game.fxMan.attachBg(14, null, 9999);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}