# La Citadelle du Jugement

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/citadelle-du-jugement.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/citadelle-du-jugement.git
```
