package framecounter;

import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;
import patchman.IPatch;
import merlin.IAction;
import etwin.Obfu;
import patchman.Ref;
import patchman.module.GameEvents;

@:build(patchman.Build.di())
class FrameCounter {


  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;

  public var pauseTimeReal: Map<String,Float> = new Map<String,Float>();
  public var gameTimeReal: Map<String,Float> = new Map<String,Float>();

  public var levelId: String;

  public var pauseStart: Float = 0;
  public var gameStart: Float = 0;

  private var gameEvents: GameEvents;

  private function gameTimerInit()   { this.gameTimeReal[this.levelId] = 0; }
  private function pauseTimerInit()  { this.pauseTimeReal[this.levelId] = 0; }
  private function gameTimerStart()  { this.gameStart = flash.Lib.getTimer(); }
  private function pauseTimerStart() { this.pauseStart = flash.Lib.getTimer(); }
  private function gameTimerStop()   { this.gameTimeReal[this.levelId] += (flash.Lib.getTimer() - this.gameStart) / 1000; }
  private function pauseTimerStop()  { this.pauseTimeReal[this.levelId] += (flash.Lib.getTimer() - this.pauseStart) / 1000; }

  public function new(gameEvents: GameEvents) {
    this.gameEvents = gameEvents;
    this.patches = FrozenArray.of(
      Ref.auto(hf.mode.GameMode.initLevel).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
        this.levelId = self.currentDim + "-" + self.world.currentId;
        if (!this.gameTimeReal.exists(this.levelId)) {
          gameTimerInit();
          pauseTimerInit();
        }
        gameTimerStart();
      }),

      Ref.auto(hf.mode.GameMode.onPause).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
        gameTimerStop();
        pauseTimerStart();
      }),
      Ref.auto(hf.mode.GameMode.onMap).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
        gameTimerStop();
        pauseTimerStart();
      }),
      Ref.auto(hf.mode.GameMode.onUnpause).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
        if (!self.fl_pause) {
          pauseTimerStop();
          gameTimerStart();
        }
      }),
      Ref.auto(hf.mode.GameMode.goto).before(function(hf: hf.Hf, self: hf.mode.GameMode, id: Int): Void {
        gameTimerStop();
      }),
      Ref.auto(hf.mode.GameMode.switchDimensionById).after(function(hf: hf.Hf, self: hf.mode.GameMode, id: Int, lid: Int, pid: Int): Void {
        gameTimerStop();
      }),


      Ref.auto(hf.mode.Adventure.saveScore).replace(function(hf: hf.Hf, self: hf.mode.Adventure): Void {
        gameTimerStop();
        this.saveData(self);
      }),
      Ref.auto(hf.mode.MultiCoop.saveScore).replace(function(hf: hf.Hf, self: hf.mode.Adventure): Void {
        gameTimerStop();
        this.saveData(self);
      }),
      Ref.auto(hf.mode.Adventure.exitGame).replace(function(hf: hf.Hf, self: hf.mode.Adventure): Void {
        self.saveScore();
      })
    );
  }

  private function saveData(game: hf.mode.GameMode): Void {
    var maxLevel: Int = game.dimensions[0].currentId;
    var itemsTaken: Array<Int> = game.getPicks2();
    var items = [for (i in 0...itemsTaken.length) {
      if (itemsTaken[i] != null)
        Std.string(i) => itemsTaken[i];
    }];

    var totalGameTime: Float = 0;
    for (levelGameTime in gameTimeReal) {
      totalGameTime += levelGameTime;
    }
    var totalGameTimeI: Int = Std.int(totalGameTime);
    var totalGameTimeStr: String = Std.int(totalGameTimeI / 3600) + ":" + Std.int(totalGameTimeI / 60) % 60 + ":" + totalGameTimeI % 60;

    var totalPauseTime: Float = 0;
    for (levelPauseTime in pauseTimeReal) {
      totalPauseTime += levelPauseTime;
    }
    var totalPauseTimeI: Int = Std.int(totalPauseTime);
    var totalPauseTimeStr: String = Std.int(totalPauseTimeI / 3600) + ":" + Std.int(totalPauseTimeI / 60) % 60 + ":" + totalPauseTimeI % 60;

    gameEvents.endGame(new ef.api.run.SetRunResultOptions(!game.fl_gameOver, maxLevel, game.savedScores, items, {
      var obj = {};
      Obfu.setField(obj, "gtr", gameTimeReal);
      Obfu.setField(obj, "ptr", pauseTimeReal);
      Obfu.setField(obj, "tgtr", totalGameTimeStr);
      Obfu.setField(obj, "tptr", totalPauseTimeStr);
      obj;
    }));
  }
}