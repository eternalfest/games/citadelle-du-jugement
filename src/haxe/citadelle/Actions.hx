package citadelle;

import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IRefFactory;

import citadelle.actions.SetupLives;
import better_script.NoNextLevel;

@:build(patchman.Build.di())
class Actions {
  @:diExport
  public var patch(default, null): IPatch;
  @:diExport
  public var setupLives(default, null): IAction;
  @:diExport
  public var noNextLevel(default, null): IRefFactory;

  public function new() {
    this.setupLives = new SetupLives();

    var noNextLevel = new NoNextLevel();
    this.patch = noNextLevel.patch;
    this.noNextLevel = noNextLevel.merlinRef;
  }
}
